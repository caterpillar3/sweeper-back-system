import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import Admin from '@/layouts/admin.vue';
import Index from '@/pages/index.vue';
import Login from '@/pages/login.vue';
import NotFound from '@/pages/404.vue';
import About from '@/pages/about.vue';
import Bing from '@/pages/link/bing/index.vue';
import CSDN from '@/pages/link/csdn/index.vue';
import Ai from '@/pages/link/chatglm/index.vue';
import Banneds from '@/pages/users/banneds.vue';
import Players from '@/pages/users/players.vue';
import Users from '@/pages/users/users.vue';
import Game2048 from '@/pages/recreation/game_2048/index.vue';
import Snake from '@/pages/recreation/snake/index.vue';
import BeatPlane from '@/pages/recreation/BeatPlane/index.vue';

// 配置路由
const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    component: Admin,
    //子路由
    children: [
      {
        path: "/",
        component: Index,
        meta: {
          title: "主控台"
        }
      },
      {
        path: "/2048",
        component: Game2048,
        meta: {
          title: "2048"
        }
      },
      {
        path: "/snake",
        component: Snake,
        meta: {
          title: "贪食蛇"
        }
      },
      {
        path: "/beatPlane",
        component: BeatPlane,
        meta: {
          title: "打飞机"
        }
      },
      {
        path: "/bing",
        component: Bing,
        meta: {
          title: "Bing 内嵌"
        }
      },
      {
        path: "/ai",
        component: Ai,
        meta: {
          title: "智谱清言"
        }
      },
      {
        path: "/csdn",
        component: CSDN,
        meta: {
          title: "CSDN 个人主页"
        }
      },
      {
        path: "/users",
        component: Users,
        meta: {
          title: "管理员"
        }
      },
      {
        path: "/players",
        component: Players,
        meta: {
          title: "玩家管理"
        }
      },
      {
        path: "/banneds",
        component: Banneds,
        meta: {
          title: "封禁管理"
        }
      },
      {
        path: "/about",
        component: About,
        meta: {
          title: "关于"
        }
      },
    ]

  },
  {
    path: "/login",
    component: Login,
    meta: {
      title: "登录"
    }
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: NotFound,
    meta: {
      title: "404 NotFound"
    }
  },
];


// 返回一个 router 实列，为函数，里面有配置项（对象） history
const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

// 3导出路由   然后去 main.ts 注册 router.ts
export default router