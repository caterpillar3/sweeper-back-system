import { createStore } from "vuex";
import managerApi from "@/api/ManagerApi";
import auth from "@/composables/Auth";
import ManagerApi from "@/api/ManagerApi";
import Util from "@/composables/Util";


// 创建一个新的 store 实例
const store = createStore({
  state() {
    return {
      // 用户信息
      user: undefined,
      // 是否展开
      isUnfold: true,
      // 是否身份
      characters: "",
    }
  },
  mutations: {
    //记录用户信息
    SET_USERINFO(state, user) {
      state.user = user;
      state.characters = user.characters
    },
    //展开/收起侧边栏
    handleAsideWidth(state) {
      state.isUnfold = !state.isUnfold;
    },
  },
  actions: {
    // 获取当前登录用户信息
    getinfo({ commit }) {
      return new Promise((resolve, reject) => {
        managerApi.getInst().getinfo().then(res => {
          if (res.status.code == 200) {
            commit("SET_USERINFO", res.data);
          } else {
            Util.getInst().toast(res.status.msg, "error");
          }
          resolve(res);
        }).catch(error => {
          reject(error);
        });
      })
    },
    logout({ commit }) {
      auth.getInst().removeToken();
      commit("SET_USERINFO", {});
    }
  }
})

export default store;