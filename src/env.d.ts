import { ComponentCustomProperties } from '@/vue'
import { Store } from 'vuex'

declare module "qrcodejs2" {
  export default require('qrcodejs2')
}

declare module '@vue/runtime-core' {
  // declare your own store states
  interface State {
    [x: string]: any
    orbital
  }

  // provide typings for `this.$store`
  interface ComponentCustomProperties {
    $store: Store<State>
  }
}
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}