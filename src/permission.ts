import router from "@/router";
import Auth from "@/composables/Auth";
import Util from "@/composables/Util";
import store from "./store";

// 全局前置守卫
let hasGetInfo: boolean = false;
router.beforeEach(async (to, from, next) => {
    //显示loading
    Util.getInst().showFullLoading();

    const token = Auth.getInst().getToken();

    //检查登录状态
    if (!token && to.path != "/login") {
        Util.getInst().toast("请先登录", "error");
        return next({ path: "/login" });
    }
    //防止重复登录
    if (token && to.path == "/login") {
        Util.getInst().toast("请勿重复登陆", "error");
        return next({ path: from.path ? from.path : "/" });
    }

    //如果用户登录了，自动获取用户信息
    if (token && !hasGetInfo) {
        let iserror: boolean = false;
        hasGetInfo = true;
        await store.dispatch("getinfo").then((res) => {
            if (res.status.code != 200) {
                Auth.getInst().removeToken();
                store.commit("SET_USERINFO", {});
                iserror = true;
            }
        }).finally(() => hasGetInfo = false);
        if (iserror)
            return next({ path: "/login" });
    }

    let title: string = (to.meta.title ?? "") + "-毛毛虫网络管理系统";
    document.title = title;
    next();
})

// 全局后置守卫
router.afterEach((to, from) => { Util.getInst().hideFullLoading() })