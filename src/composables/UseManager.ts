import ManagerApi from "@/api/ManagerApi";
import Util from "./Util";
import { useStore } from "vuex";
import { useRouter } from "vue-router";
import { FormInstance, FormRules } from "element-plus";
import { reactive, ref } from "vue";

export default class UseManager {
    private static inst: UseManager | null = null;

    static getInst(): UseManager {
        if (!this.inst) {
            this.inst = new UseManager();
        }
        return this.inst;
    }

    /**
     * 修改密码
     * @returns 
     */
    useRepassword() {
        const router = useRouter();
        const store = useStore();

        const formDrawerRef = ref(null);

        interface RuleForm {
            oldpassword: string;
            password: string;
            repassword: string;
        }

        const ruleFormRef = ref<FormInstance | null>(null);
        // do not use same name with ref
        const form = reactive<RuleForm>({
            oldpassword: "",
            password: "",
            repassword: "",
        });

        const rules = reactive<FormRules<RuleForm>>({
            oldpassword: [
                {
                    required: true,
                    message: "旧密码不能为空",
                    trigger: "blur",
                },
                {
                    min: 5,
                    max: 20,
                    message: "密码长度在5-20个字符",
                    trigger: "blur",
                },
            ],
            password: [
                {
                    required: true,
                    message: "新密码不能为空",
                    trigger: "blur",
                },
                {
                    min: 5,
                    max: 20,
                    message: "密码长度在5-20个字符",
                    trigger: "blur",
                },
            ],
            repassword: [
                {
                    required: true,
                    message: "确认密码不能为空",
                    trigger: "blur",
                },
                {
                    min: 5,
                    max: 20,
                    message: "密码长度在5-20个字符",
                    trigger: "blur",
                },
            ],
        });

        const onSubmit = async () => {
            await ruleFormRef.value?.validate(async (valid, fields) => {
                if (!valid) {
                    return false;
                }
                const showLoading: Function = formDrawerRef.value!["showLoading"];
                showLoading();
                ManagerApi
                    .getInst()
                    .updatepassword(form)
                    .then((res) => {
                        if (!res) return;
                        Util.getInst().toast("修改密码成功,请重新登录");
                        store.dispatch("logout");
                        router.push("/login");
                    })
                    .finally(() => {
                        const hideLoading: Function = formDrawerRef.value!["hideLoading"];
                        hideLoading();
                    });
            });
        };

        const openRePasswordForm: Function = () => {
            const open: Function = formDrawerRef.value!["open"];
            open();
        };

        return {
            formDrawerRef,
            ruleFormRef,
            form,
            rules,
            onSubmit,
            openRePasswordForm
        }
    }

    /**
     * 退出登录
     * @returns
     */
    useLogout(): Function {
        const router = useRouter();
        const store = useStore();
        return ()=>{
            Util.getInst()
                .showModel("是否退出登录")
                .then((res) => {
                    ManagerApi.getInst()
                        .logout()
                        .finally(() => {
                            store.dispatch("logout");
                            router.push("/login");
                            Util.getInst().toast("退出登录成功");
                        });
                });
        }
    }
}