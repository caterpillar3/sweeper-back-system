import { ElNotification, ElMessageBox, MessageBoxData } from 'element-plus';
import { EpPropMergeType } from 'element-plus/es/utils';
import nprogress from 'nprogress'

export interface DateInfo {
    year: number,
    month: number,
    day: number,
    hour: number,
    minutes: number,
    seconds: number
}
type msgType = EpPropMergeType<StringConstructor, "" | "success" | "warning" | "error" | "info", unknown>;
export default class Util {
    private static inst: Util | null = null;

    static getInst(): Util {
        if (!this.inst) {
            this.inst = new Util();
        }
        return this.inst;
    }

    /**
     * 消息提示
     * @param message 消息
     * @param type 消息类型
     * @param dangerouslyUseHTMLString 是否作HTML处理
     * @param duration 显示时长
     */
    toast(message: string, type: msgType | undefined = "success", dangerouslyUseHTMLString: boolean = false, duration: number = 3000) {
        ElNotification({
            message,
            type,
            dangerouslyUseHTMLString,
            duration
        });
    }

    /**
     * 确认框
     * @param content 内容
     * @param type 类型
     * @param title 标题
     * @returns 
     */
    showModel(content: string, type: msgType | undefined = "warning", title: string = ""): Promise<MessageBoxData> {
        return ElMessageBox.confirm(
            content,
            title,
            {
                confirmButtonText: '确认',
                cancelButtonText: '取消',
                type,
            }
        )
    }

    /**
     * 显示全屏loading
     */
    showFullLoading() {
        nprogress.start();
    }

    /**
     * 隐藏全屏loading
     */
    hideFullLoading() {
        nprogress.done();
    }

    /**
     * 时间转换
     * @param date 
     */
    convertDateToInfo(date: Date): DateInfo {
        return {
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            day: date.getDate(),
            hour: date.getHours(),
            minutes: date.getMinutes(),
            seconds: date.getSeconds()
        };
    }
}