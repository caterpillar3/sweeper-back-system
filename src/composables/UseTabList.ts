import { Ref, ref, UnwrapRef } from 'vue';
import { onBeforeRouteUpdate, RouteLocationNormalized, Router, useRouter } from 'vue-router';
import { useCookies } from '@vueuse/integrations/useCookies';
import { Cookie } from 'universal-cookie';
import { useRoute, RouteLocationNormalizedLoaded } from 'vue-router';

export interface Tabtemplate {
    title: string | unknown,
    path: string
}

export default class UseTabList {
    private static inst: UseTabList | null = null;
    cookie: Cookie = useCookies();
    route: RouteLocationNormalizedLoaded = useRoute();
    router: Router = useRouter()

    static getInst(): UseTabList {
        if (!this.inst) {
            this.inst = new UseTabList();
        }
        return this.inst;
    }

    activeTab: Ref<string> = ref(this.route.fullPath);
    tabList: Ref<UnwrapRef<Array<Tabtemplate>>> = ref([
        {
            title: '主控台',
            path: "/"
        }
    ])

    /**
     * 关闭标签栏
     */
    handleClose: Function = (command: string) => {
        this.tabList.value = this.tabList.value.filter(tab => tab.path == "/");
        switch (command) {
            case "clearOther":
                if (this.route.fullPath != "/") {
                    const newTab: Tabtemplate = {
                        title: this.route.meta.title,
                        path: this.route.fullPath
                    };
                    this.tabList.value.push(newTab);
                }
                break;
            case "clearAll":
                this.router.push("/");
                break;
        }
        this.cookie.set("tabList", this.tabList.value);
    }
    /**
     * 切换tag
     * @param path 路径
     */
    changeTab: Function = (path: string) => {
        this.router.push(path);
    }

    /**
     * 移除tag
     * @param path 路径
     */
    removeTab: Function = (path: string) => {
        let onPath: string = this.activeTab.value;
        const index: number = this.tabList.value.findIndex(tab => tab.path == path);
        if (onPath == path) {
            const toIndex: number = index == this.tabList.value.length - 1 ? index - 1 : index + 1;
            this.router.push(this.tabList.value[toIndex].path);
        }
        this.tabList.value.splice(index, 1);
        this.cookie.set("tabList", this.tabList.value);
    }

    /**
     * 初始化taglist
     */
    initTabList() {
        let tbs: Array<Tabtemplate> = this.cookie.get("tabList");
        if (tbs)
            this.tabList.value = tbs;
    }

    /**
     * 添加tag
     * @param newTab 新tag
     */
    addTab(newTab: Tabtemplate) {
        const path: Tabtemplate | undefined = this.tabList.value.find(tab => tab.path == newTab.path);
        if (!path)
            this.tabList.value.push(newTab);
        this.cookie.set("tabList", this.tabList.value);
    }



    setup() {
        onBeforeRouteUpdate((to: RouteLocationNormalized, from: RouteLocationNormalized) => {
            const newTab: Tabtemplate = {
                title: to.meta.title,
                path: to.path
            };
            this.addTab(newTab);
            this.activeTab.value = newTab.path;
        });
        this.initTabList();
        return {
            tabList: this.tabList,
            activeTab: this.activeTab,
            handleClose: this.handleClose,
            changeTab: this.changeTab,
            removeTab: this.removeTab,
        };
    }
}