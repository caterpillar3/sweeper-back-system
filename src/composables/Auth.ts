import { useCookies } from "@vueuse/integrations/useCookies";
import { Cookie } from "universal-cookie";
const TokenKey = "admin-token"

export default class Auth {
    private cookie: Cookie | null = null;
    private static inst: Auth | null = null;

    static getInst(): Auth {
        if (!this.inst) {
            this.inst = new Auth();
            this.inst.cookie = useCookies();
        }
        return this.inst;
    }

    //获取token
    getToken() {
        return this.cookie.get(TokenKey);
    }

    //设置token
    setToken(token: string) {
        this.cookie.set(TokenKey, token)
    }

    //清除token
    removeToken() {
        this.cookie.remove(TokenKey);
    }
}