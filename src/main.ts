import { createApp } from 'vue';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import App from '@/App.vue';
import 'virtual:windi.css';
import router from '@/router';
import store from '@/store/index';
import '@/permission';
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import "nprogress/nprogress.css"

const app = createApp(App)

app.use(ElementPlus);
app.use(store);
app.use(router);
app.mount('#app');

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}