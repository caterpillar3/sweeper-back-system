import { service } from '@/axios'
import { number } from 'echarts';
import { da } from 'element-plus/es/locale';

interface PageRequest {
    pageNumber: number,
    pageSize: number,
    data?: any
}

export default class ManagerApi {
    private static inst: ManagerApi | null = null;

    static getInst(): ManagerApi {
        if (!this.inst)
            this.inst = new ManagerApi();
        return this.inst;
    }

    /**
     * 登录
     * @param username 用户名
     * @param password 密码
     * @returns 
     */
    async login(form: { username: string, password: string }) {
        return service.post("/admin/login", form);
    }

    /**
     * 获取用户信息
     * @returns 
     */
    getinfo() {
        return service.post("/admin/getinfo");
    }

    /**
     * 退出登录
     * @returns 
     */
    logout() {
        return service.post("/admin/logout");
    }

    /**
     * 修改密码
     * @param data 
     * @returns 
     */
    updatepassword(data: any) {
        return service.post("/admin/updatepassword", data);
    }

    /**
     * 新建管理员
     * @param username 
     * @returns 
     */
    createAdmin(data: any) {
        return service.post("/admin/createAdmin", data);
    }

    /**
     * 分页获取管理员数据
     * @param username 
     * @returns 
     */
    getAdminInfoList(pageNumber: number, pageSize: number) {
        return service.get(`/admin/getAdminInfoList?pageNumber=${pageNumber}&pageSize=${pageSize}`);
    }

    /**
     * 修改管理员状态
     * @param username 
     * @returns 
     */
    setAdminStatus(data: any) {
        return service.put("/admin/setStatus", data);
    }

     /**
     * 修改玩家封禁状态
     * @param username 
     * @returns 
     */
     setPlayerStatus(data: any) {
        return service.put("/playerTime/setStatus", data);
    }

    /**
     * 获取玩家封禁信息
     * @param pageNumber 
     * @param pageSize 
     * @returns 
     */
    getBannerInfoList(pageNumber: number, pageSize: number, name: string) {
        if(name)
            return service.get(`/playerInfo/getBannedByNameList?pageNumber=${pageNumber}&pageSize=${pageSize}&name=${name}`);
        return service.get(`/playerInfo/getBannedList?pageNumber=${pageNumber}&pageSize=${pageSize}`);
    }

    /**
     * 获取用户信息
     * @param pageNumber 
     * @param pageSize 
     * @returns 
     */
    getUserInfoList(pageNumber: number, pageSize: number, name: string) {
        if(name)
            return service.get(`/user/getBannedByNameList?pageNumber=${pageNumber}&pageSize=${pageSize}&name=${name}`);
        return service.get(`/user/getUserInfoList?pageNumber=${pageNumber}&pageSize=${pageSize}`);
    }

    /**
     * 重置管理员密码
     * @param id 
     * @returns 
     */
    resetAdminPwd(id: number){
        return service.put(`/admin/resetPwd/${id}`);
    }

    /**
     * 重置用户密码
     * @param id 
     * @returns 
     */
    resetUserPwd(id: string){
        return service.put(`/users/resetPwd/${id}`);
    }

    /**
     * 
     * @param id 获取角色信息
     * @returns 
     */
    getRoleInfo(id: string){
        return service.get(`/playerInfo/getDialogInfoVo/${id}`);
    }

     /**
     * 修改密码
     * @param data 
     * @returns 
     */
     initAdmin(data: any) {
        return service.put("/admin/initAdmin", data);
    }
}