import axios from "axios"
import auth from "@/composables/Auth";
import util from "@/composables/Util";
import store from "./store";

export const service = axios.create({
  baseURL: "/api"
});

// 添加请求拦截器
service.interceptors.request.use(function (config) {
  // 请求时携带token
  const token = auth.getInst().getToken();
  if (token) {
    config.headers["token"] = token;
  }
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
service.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  return response.data;
}, function (error) {
  const msg: string = error.response.data.msg || "请求失败";
  if(msg == "非法token，请先登录！"){
    store.dispatch("logout").finally(()=>location.reload());
  }
  util.getInst().toast(msg, "error");
});

export default service