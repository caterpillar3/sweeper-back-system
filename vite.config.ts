import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import WindiCSS from 'vite-plugin-windicss'
import dayjs from "dayjs";
import pkg from "./package.json";

const { dependencies, devDependencies, name, version } = pkg;
export const __APP_INFO__ = {
  pkg: { dependencies, devDependencies, name, version },
  lastBuildTime: dayjs().format("YYYY-MM-DD HH:mm:ss")
};

import { join } from 'path'
function resolve(dir: string) {
  return join(__dirname, dir)
}

// https://vitejs.dev/config/
export default defineConfig({
  resolve:{
    alias: {
      '@': resolve('src'),
    },
  },

  server:{
    proxy:{
      '/api': {
        target: 'http://127.0.0.1:7080',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ''),
      },
    }
  },
  plugins: [
    vue(),
    WindiCSS()
  ],
})
